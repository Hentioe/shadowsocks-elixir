defmodule Socks5 do
  @ver_socks5 5
  @method_none 0
  @cmd_connect 1
  @atyp_ipv4 1
  @atyp_domain 3
  @rep_success 0

  @moduledoc """
    Socks5 协议实现
  """

  @doc """
    处理客户端握手
  """
  def handshake(socket) do
    case :gen_tcp.recv(socket, 0) do
      # Socks5 | 无认证
      {:ok, <<@ver_socks5, nmethod::integer-size(8), methods::bytes-size(nmethod)>>} ->
        <<0>> = methods
        :gen_tcp.send(socket, <<@ver_socks5, @method_none>>)
        {socket}

      _ ->
        exit(:shutdown)
    end
  end

  @doc """
    读取客户端连接请求中的信息
  """
  def read_connect_info({socket}) do
    case :gen_tcp.recv(socket, 0) do
      {:ok, <<@ver_socks5, @cmd_connect, 0, info::binary>>} ->
        {:ok, address, port} = read_coninfo(info)
        {socket, address, port}

      _ ->
        {:error, :unsupport_conncmd}
    end
  end

  # 读取连接信息
  defp read_coninfo(info) do
    case info do
      <<@atyp_domain, dlength, domain::binary-size(dlength), port::size(16)>> ->
        {:ok, domain, port}

      <<@atyp_ipv4, 4, ipv4::binary-size(4), port::size(16)>> ->
        {:ok, ipv4, port}

      _ ->
        {:error, :unsupport_coninfo}
    end
  end

  @doc """
    响应连接状态
  """
  def connected_response({socket, address, port}) do
    :gen_tcp.send(socket, <<@ver_socks5, @rep_success, 0, 1, 0, 0, 0, 0, 0::size(16)>>)
    {:ok, address, port}
  end

  @doc """
    转发数据
  """
  def forwarding(task_supervisor, target, client) do
    {:ok, sender_pid} =
      Task.Supervisor.start_child(task_supervisor, fn -> forward(client, target) end)

    :gen_tcp.controlling_process(client, sender_pid)

    {:ok, receiver_pid} =
      Task.Supervisor.start_child(task_supervisor, fn -> forward(target, client) end)

    :gen_tcp.controlling_process(target, receiver_pid)
  end

  # 转发过程
  defp forward(from, to) do
    with {:ok, data} <- :gen_tcp.recv(from, 0),
         :ok <- :gen_tcp.send(to, data) do
      forward(from, to)
    else
      _ ->
        :gen_tcp.close(from)
        :gen_tcp.close(to)
    end
  end
end
