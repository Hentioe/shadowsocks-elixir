defmodule Cipher do
  @zero_iv to_string(:string.chars(0, 16))
  @aes_block_size 16

  def encrypt(data, key) do
    :crypto.block_encrypt(:aes_cbc128, gen_norm_key(key), @zero_iv, pad(data, @aes_block_size))
  end

  def decrypt(data, key) do
    padded = :crypto.block_decrypt(:aes_cbc128, gen_norm_key(key), @zero_iv, data)
    unpad(padded)
  end

  defp pad(data, block_size) do
    to_add = block_size - rem(byte_size(data), block_size)
    data <> to_string(:string.chars(to_add, to_add))
  end

  defp unpad(data) do
    to_remove = :binary.last(data)
    :binary.part(data, 0, byte_size(data) - to_remove)
  end

  defp gen_norm_key(key) do
    if String.length(key) == 16 do
      key
    else
      :erlang.md5(key)
    end
  end
end
