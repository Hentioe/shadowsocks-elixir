defmodule CipherTest do
  use ExUnit.Case

  test "encrypt and decrypt" do
    data = "Hello"
    key = "demo123"
    cipher_data = Cipher.encrypt(data, key)
    raw_data = Cipher.decrypt(cipher_data, key)
    assert data == raw_data
  end
end
