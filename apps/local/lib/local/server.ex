defmodule Local.Server do
  @default_port 1080
  @listen_opts [:binary, packet: 0, active: false, reuseaddr: true]
  @connect_opts [:binary, packet: 0, active: false]

  def start_link(opts) do
    name = Keyword.get(opts, :name, __MODULE__)
    {:ok, pid} = Task.start_link(fn -> accept(opts) end)
    Process.register(pid, name)
    {:ok, pid}
  end

  def accept(opts) do
    port = Keyword.get(opts, :port, @default_port)
    password = Keyword.get(opts, :password)
    {:ok, lsock} = :gen_tcp.listen(port, @listen_opts)
    loop_acceptor(lsock, %{password: password})
  end

  def loop_acceptor(lsock, config) do
    {:ok, client} = :gen_tcp.accept(lsock)

    {:ok, pid} =
      Task.Supervisor.start_child(Local.ConnSupervisor, fn -> serve(client, config) end)

    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(lsock, config)
  end

  def serve(client, %{password: password}) do
    conninfo =
      client
      |> Socks5.handshake()
      |> Socks5.read_connect_info()
      |> Socks5.connected_response()

    with {:ok, address, port} <- conninfo,
         {:ok, remote} <- :gen_tcp.connect('127.0.0.1', 8080, @connect_opts) do
      info = "#{address}:#{port}"
      l = String.length(info)
      encrypt_data = fn data -> Cipher.encrypt(data, password) end
      :gen_tcp.send(remote, encrypt_data.(<<l, info::binary>>))
      Socks5.forwarding(Local.ConnSupervisor, client, remote)
    else
      _ -> exit(:shutdown)
    end
  end

  def child_spec(args) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [args]},
      restart: :permanent
    }
  end
end
