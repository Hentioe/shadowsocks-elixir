defmodule Local.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, [], opts)
  end

  def init(_) do
    port = 1080
    password = "demo123"

    children = [
      {Task.Supervisor, name: Local.ConnSupervisor},
      {Local.Server, port: port, password: password}
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.init(children, opts)
  end
end
