defmodule Server.Listener do
  @default_port 8080
  @listen_opts [:binary, packet: 0, active: false, reuseaddr: true]
  @connect_opts [:binary, packet: 0, active: false]

  def start_link(opts) do
    port = Keyword.get(opts, :port, @default_port)
    name = Keyword.get(opts, :name, __MODULE__)
    {:ok, pid} = Task.start_link(fn -> accept(port) end)
    Process.register(pid, name)
    {:ok, pid}
  end

  def accept(port) do
    {:ok, lsock} = :gen_tcp.listen(port, @listen_opts)
    loop_acceptor(lsock)
  end

  def loop_acceptor(lsock) do
    {:ok, client} = :gen_tcp.accept(lsock)
    {:ok, pid} = Task.Supervisor.start_child(Server.ConnSupervisor, fn -> serve(client) end)
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(lsock)
  end

  def serve(client) do
    client
    |> read_connect_info()
    |> connect()
  end

  defp read_connect_info(client) do
    case :gen_tcp.recv(client, 0) do
      {:ok, data} ->
        decrypt_data = fn data -> Cipher.decrypt(data, "demo123") end
        <<length, info::binary-size(length), _rest::binary>> = decrypt_data.(data)
        {:ok, client, info}

      _ ->
        {:error, :read_connect_info_failed}
    end
  end

  defp connect({:ok, client, info}) do
    {address, port} = parse_coninfo(info)

    with {:ok, target} <- :gen_tcp.connect(address, port, @connect_opts) do
      Socks5.forwarding(Server.ConnSupervisor, client, target)
    else
      _ -> {:error, :connect_failed}
    end
  end

  defp parse_coninfo(info) do
    [address, port] = String.split(info, ":")
    address = to_charlist(address)
    {port, _} = Integer.parse(port)
    {address, port}
  end

  def child_spec(args) do
    %{
      id: __MODULE__,
      start: {__MODULE__, :start_link, [args]},
      restart: :permanent
    }
  end
end
