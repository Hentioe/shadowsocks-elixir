defmodule Server.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, [], opts)
  end

  def init(_) do
    port = 8080

    children = [
      {Task.Supervisor, name: Server.ConnSupervisor},
      {Server.Listener, port: port}
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]
    Supervisor.init(children, opts)
  end
end
