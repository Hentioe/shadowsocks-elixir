defmodule Server.Application do
  use Application

  def start(_type, _args) do
    case Server.Supervisor.start_link([]) do
      {:ok, pid} -> {:ok, pid}
      error -> {:error, error}
    end
  end
end
